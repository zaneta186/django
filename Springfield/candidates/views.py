from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from .models import Candidate
from django.http import HttpResponseRedirect



class IndexView(generic.ListView):
    model = Candidate
    template_name = 'candidates/index.html'
    context_object_name = 'candidates'


class DetailView(generic.DetailView):
    model = Candidate
    template_name = 'candidates/detail.html'


def vote(request, candidate_id):
    candidate = get_object_or_404(Candidate, pk=candidate_id)
    candidate.votes += 1
    candidate.save()
    return HttpResponseRedirect(reverse('candidates:detail', args=(candidate.id,)))
