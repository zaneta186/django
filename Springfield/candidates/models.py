from django.contrib.gis.db import models


class Candidate(models.Model):
    name = models.CharField(max_length=50)
    photo = models.ImageField()
    text = models.TextField()
    location = models.PointField()
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.name
