from django.urls import path

from . import views

app_name = 'candidates'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:candidate_id>/vote/', views.vote, name='vote'),
]
