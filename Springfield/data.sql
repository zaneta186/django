--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2018-12-27 16:37:05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4406 (class 0 OID 18005)
-- Dependencies: 219
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- TOC entry 4402 (class 0 OID 17987)
-- Dependencies: 215
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	candidates	candidate
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
\.


--
-- TOC entry 4404 (class 0 OID 17997)
-- Dependencies: 217
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add candidate	1	add_candidate
2	Can change candidate	1	change_candidate
3	Can delete candidate	1	delete_candidate
4	Can view candidate	1	view_candidate
5	Can add log entry	2	add_logentry
6	Can change log entry	2	change_logentry
7	Can delete log entry	2	delete_logentry
8	Can view log entry	2	view_logentry
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add group	4	add_group
14	Can change group	4	change_group
15	Can delete group	4	delete_group
16	Can view group	4	view_group
17	Can add user	5	add_user
18	Can change user	5	change_user
19	Can delete user	5	delete_user
20	Can view user	5	view_user
21	Can add content type	6	add_contenttype
22	Can change content type	6	change_contenttype
23	Can delete content type	6	delete_contenttype
24	Can view content type	6	view_contenttype
25	Can add session	7	add_session
26	Can change session	7	change_session
27	Can delete session	7	delete_session
28	Can view session	7	view_session
\.


--
-- TOC entry 4408 (class 0 OID 18015)
-- Dependencies: 221
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 4410 (class 0 OID 18023)
-- Dependencies: 223
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- TOC entry 4412 (class 0 OID 18033)
-- Dependencies: 225
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- TOC entry 4414 (class 0 OID 18041)
-- Dependencies: 227
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 4418 (class 0 OID 18131)
-- Dependencies: 231
-- Data for Name: candidates_candidate; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.candidates_candidate (id, name, photo, text, location, votes) FROM stdin;
2	Moe Szyslak	moe_acMlbCo.jpg	Za prvé jsem pro zlepšení podmínek pro všechny podnikatele města Springfield, čeho chci dosáhnout pomocí snížení daňového zatížení a zvýšením příspěvků od státu pro podnikatele. Za druhé jsem pro zrušení zákazu kouření v barech a hospodách. Dále bych chtěl prosadit levnější nájemné v celém našem městě, aby si lidé mohli ušetřit více peněz z jejich výplat. Za třetí jsem pro legalizaci zbraní, které by měli sloužit obyvatelům k nutné obraně. Důvodem stále vzrůstající kriminalita, a tudíž potřeba se bránit případnému napadení. Mým cílem je také zpřísnění trestů za šikanu a veřejné posměšky.	0101000020E6100000FEFFF6FF512652C06BC9EB9432104540	2
3	Joe Quimby	Mayor-quimby.jpg	Opět se blíží volby, a proto bych vám chtěl představit svůj program. Na začátek bych vám chtěl říct, že pokud mě opět zvolíte za svého starostu, budu se nadále snažit zvyšovat životní úroveň našich občanů, dále budu bojovat proti korupci a úplatkářství ve vedení našeho města. Zaměřím se na podporu školství a vzdělanosti, tím že místní základní škole poskytnu dotace na vybavení. Abych podpořil místní podnikatele, snížím jim nájem na využívání městských budov. Ostatní kandidáti mají sice ambiciózní volební programy, avšak nemají takové zkušenosti s vedením města, jako já, proto volte Quimbyho!	0101000020E6100000FF7FFBFF122652C0ACA84CA5980D4540	2
1	Homer Simpson	Homer_l30D315.jpg	Za prvé bych chtěl snížit daně fyzických osob, aby měli lidé více peněz, a dále spotřební daně, a to zejména z piva, aby bylo levnější a lidé si ho mohli více vychutnávat. Za druhé chci zavést povinné přestávky během pracovní doby, aby lidé při práci neusínali. Dále bych chtěl zlepšit kvalitu obědů pro pracovníky a děti ve škole, což zahrnuje větší porce a více druhů jídel. Za čtvrté bych zavedl neomezený přísun koblih na pracovišti zdarma. Mým cílem je zlepšit bezpečnost ve Springfieldu a to jednak prostřednictvím snížení kriminality a také zvýšením bezpečnostních opatření v jaderné elektrárně pro kterou já sám pracuju jako bezpečnostní technik.	0101000020E6100000FFFFFF7F292652C0BBDAAA529F0D4540	4
\.


--
-- TOC entry 4416 (class 0 OID 18101)
-- Dependencies: 229
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- TOC entry 4400 (class 0 OID 17976)
-- Dependencies: 213
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-12-02 21:06:54.653154+01
2	auth	0001_initial	2018-12-02 21:06:54.747696+01
3	admin	0001_initial	2018-12-02 21:06:54.7749+01
4	admin	0002_logentry_remove_auto_add	2018-12-02 21:06:54.782879+01
5	admin	0003_logentry_add_action_flag_choices	2018-12-02 21:06:54.789895+01
6	contenttypes	0002_remove_content_type_name	2018-12-02 21:06:54.807858+01
7	auth	0002_alter_permission_name_max_length	2018-12-02 21:06:54.813842+01
8	auth	0003_alter_user_email_max_length	2018-12-02 21:06:54.820822+01
9	auth	0004_alter_user_username_opts	2018-12-02 21:06:54.826806+01
10	auth	0005_alter_user_last_login_null	2018-12-02 21:06:54.835782+01
11	auth	0006_require_contenttypes_0002	2018-12-02 21:06:54.83678+01
12	auth	0007_alter_validators_add_error_messages	2018-12-02 21:06:54.844758+01
13	auth	0008_alter_user_username_max_length	2018-12-02 21:06:54.860716+01
14	auth	0009_alter_user_last_name_max_length	2018-12-02 21:06:54.868696+01
15	candidates	0001_initial	2018-12-02 21:06:54.884651+01
16	sessions	0001_initial	2018-12-02 21:06:54.897637+01
\.


--
-- TOC entry 4419 (class 0 OID 18141)
-- Dependencies: 232
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: django
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- TOC entry 4200 (class 0 OID 16705)
-- Dependencies: 198
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys  FROM stdin;
\.


--
-- TOC entry 4425 (class 0 OID 0)
-- Dependencies: 218
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- TOC entry 4426 (class 0 OID 0)
-- Dependencies: 220
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 4427 (class 0 OID 0)
-- Dependencies: 216
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 28, true);


--
-- TOC entry 4428 (class 0 OID 0)
-- Dependencies: 224
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- TOC entry 4429 (class 0 OID 0)
-- Dependencies: 222
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- TOC entry 4430 (class 0 OID 0)
-- Dependencies: 226
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 4431 (class 0 OID 0)
-- Dependencies: 230
-- Name: candidates_candidate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.candidates_candidate_id_seq', 3, true);


--
-- TOC entry 4432 (class 0 OID 0)
-- Dependencies: 228
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 11, true);


--
-- TOC entry 4433 (class 0 OID 0)
-- Dependencies: 214
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 7, true);


--
-- TOC entry 4434 (class 0 OID 0)
-- Dependencies: 212
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: django
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 16, true);


-- Completed on 2018-12-27 16:37:07

--
-- PostgreSQL database dump complete
--

